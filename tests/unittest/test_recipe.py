import mock
import unittest2
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),"../../")))
from recipe import Recipe
#from test_utils import dotdict

class TestRecipe(unittest2.TestCase):
    def test_portion_ingredients_one_ingredient_happy(self):
        my_recipe = Recipe()
        test_str = my_recipe.portion_ingredients(['chocolate'])
        nlines = test_str.count('\n')

        # Just one item, so there should only be 1 line for the instruction
        self.assertEquals(1, nlines)
        # Line should start with a number
        self.assertEquals(True, test_str[0].isdigit())

    def test_portion_ingredients_multiple_ingredients_happy(self):
        my_recipe = Recipe()
        test_str = my_recipe.portion_ingredients(['chocolate', 'cookie'])
        nlines = test_str.count('\n')

        # Just one item, so there should only be 1 line for the instruction
        self.assertNotEquals(1, nlines)
        # Line should start with a number
        for line in test_str.splitlines():
            self.assertEquals(True, line[0].isdigit())

    @mock.patch('recipe.Recipe.get_ingredients', return_value = ['chocolate','nuts'])
    @mock.patch('recipe.Recipe.portion_ingredients', return_value = '3 Pint of chocolate \n2 Cupful of nuts \n')
    def test_add_ingredients(self, mock_portion, mock_get):
        my_recipe = Recipe()
        test_msg = my_recipe.add_ingredients()
        self.assertEquals(True, test_msg.startswith('Ingredients you need today:'))
        self.assertEquals(3, test_msg.count('\n'))

    @mock.patch('recipe.Recipe.get_ingredients', return_value = ['chocolate','nuts'])
    @mock.patch('recipe.Recipe.portion_ingredients', return_value = '3 Pint of chocolate \n2 Cupful of nuts \n')
    def test_add_ingredients_sad(self, mock_portion, mock_get):
        my_recipe = Recipe()
        test_msg = my_recipe.add_ingredients()
        self.assertNotEquals(4, test_msg.count('\n'))

    def test_add_instruction(self):
        my_recipe = Recipe()
        my_recipe.ingredients = ['Test1', 'Test2']
        my_recipe.instruction = 'BLAHBLAH'
        test_msg = my_recipe.add_instruction()
        self.assertIn('BLAH', test_msg)
        self.assertIn('Test1', test_msg)
        self.assertIn('Test2', test_msg)
        self.assertNotIn('Test3', test_msg)
  
