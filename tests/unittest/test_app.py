import os
import sys
import unittest2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),"../../")))
import recipe_generator

class TestApp(unittest2.TestCase):
    def test_generate(self):
        self.app = recipe_generator.app.test_client()
        rv = self.app.get('/generate')
        print rv.data
        self.assertIn('Ingredients you need today:', rv.data)

    def test_slash(self):
        #making sure / is redirected to home, which in turn calls generate
        self.app = recipe_generator.app.test_client()
        rv = self.app.get('/', follow_redirects=True)
        self.assertIn('Ingredients you need today:', rv.data)
