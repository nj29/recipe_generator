#!/bin/bash
set -e

#Make location of script the working directory so that the virtual environment amd results folder are created relative to it
cd ${0%/*}

virtualenv unittest_venv
. unittest_venv/bin/activate
pip install --upgrade pip
pip install -r requirement.txt
nosetests --with-xunit --xunit-file=unittest.yaml unittest
deactivate
