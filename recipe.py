#!/usr/bin/env python

import os
import logging
import random

INGREDIENTS = ['Chocolate', 'Cookies', 'Steak', 'Lobster', 'Banana', 'Apple', 'Olive', 'Spinich', 'Cheese', 'Eggs', 'Chicken', 'Shrimp', 'Bacon', 'Peanut Butter', 'Kale']
BASE =  ['Sugar', 'Milk', 'Nutella', 'Butter']
INSTRUCTION = ['Deep-fry', 'Fry', 'Slow Cook', 'Set temperature of 350F, Oven-bake', 'Stir-fry', 'Blender Mix','Sun Dry']
CONDITION = ['till golden', 'when it seems ok', 'for 6 hours', 'for 120 minutes','when you feel like done', 'til it looks mixed', 'for 3 weeks']
SIZE = ['Cupful', 'Tiny bit', 'Pint', 'Table-spoon', 'Tea-spoon', 'Handful', 'Bowl', 'Servings', 'Spoonful']
SERVING_INSTRUCTION = ['Grab your friends and share it!', 'Mix all and Serve with ice', 'Mix, Serve with wine', 'Serve on a plate']
MAX_PORTION = 8
MIN_PORTION = 1
MIN_INGREDIENT = 6

_LOG = logging.getLogger(__name__)

class Recipe(object):

    ''' recipe class to generate a proper recipe with instructions '''
    def __init__(self):
        self.recipe = ''
        self.ingredients = ''
        self.instruction = ''

    def get_ingredients(self):
        self.ingredients = random.sample(INGREDIENTS, random.randint(MIN_INGREDIENT, len(INGREDIENTS)))
        return self.ingredients

    def portion_ingredients(self,ingredients):
        ret_str = ''
        for item in ingredients:
            ret_str += str(random.randint(MIN_PORTION,MAX_PORTION)) + ' ' + random.choice(SIZE) + ' of ' + item + '\n'
        return ret_str

    # Pick a random of 1 to max ingredients 
    def add_ingredients(self):
        ingredients = self.get_ingredients()
        full_ingredients = self.portion_ingredients(ingredients)
        ret_msg = 'Ingredients you need today: \n' + full_ingredients
        return ret_msg

    def add_instruction(self):
        for item in self.ingredients:
            self.instruction += random.choice(INSTRUCTION) + ' ' + item + ' ' + random.choice(CONDITION) +'\n'
        return self.instruction + '\n' + random.choice(SERVING_INSTRUCTION)

    def generate(self):
        _LOG.info('Generating a recipe')
        self.recipe = self.add_ingredients() +'\n'
        self.recipe += self.add_instruction()
        return self.recipe
