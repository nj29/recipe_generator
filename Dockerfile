from python:2.7.13-slim
MAINTAINER Nan Jiang <n.jiang829@gmail.com>

# application folder
ENV APP_DIR /app
ENV LOG_DIR /var/recipe_generator

# app dir
RUN mkdir ${APP_DIR}
RUN mkdir ${LOG_DIR}
VOLUME [${APP_DIR}]
WORKDIR ${APP_DIR}

COPY . /app

# basic flask environment
RUN pip install --upgrade pip \
    && pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["recipe_generator.py"]
