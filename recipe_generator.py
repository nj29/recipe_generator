#!/usr/bin/env python
import os
import re
from flask import Flask, redirect, url_for, render_template, flash, json
import logging
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler
import recipe
from recipe import Recipe

app = Flask(__name__)
FORMATTER = logging.Formatter("[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
app.secret_key = 'some_secret'

@app.route('/')
def base():
    app.logger.info('Hitting /, redirecting to home')
    return redirect(url_for('home'))

@app.route('/home')
def home():
    app.logger.info('Hitting /home api')
    flash(generate())
    return render_template('index.html')

@app.route('/generate', methods=['GET'])
def generate():
    app.logger.info('Generate recipe requested')
    my_recipe = Recipe()
    ret = my_recipe.generate().replace('\n', '<br />')
    return ret

if __name__ == '__main__':
    handler = TimedRotatingFileHandler('/var/recipe_generator/app.log', when='H', interval=1)
    handler.suffix = "%Y.%m.%d.%H"
    handler.setLevel(logging.INFO)
    handler.setFormatter(FORMATTER)
    #Add your file loggers here
    loggers = [app.logger, recipe._LOG]
    for logger in loggers:
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)
    app.debug = True
    app.logger.info('App starting')
    app.run(host='0.0.0.0', port=5000)
